
// Required modules
var http = require('http');
var url = require('url');
var path = require('path');
var fs = require('fs');

// Array of Mime Types
var mimeTypes = {
    'html'          : 'text/html',
    'jpeg'          : 'image/jpeg',
    'jpg'           : 'image/jpeg',
    'png'           : 'image/png',
    'javascript'    : 'text/javascript',
    'css'           : 'text/css'
};

// Create Server
http.createServer(function(req, res) {
    var uri = url.parse(req.url).pathname;
    var filename = path.join(process.cwd(), unescape(uri));
    console.log('Loading ' + uri);
    var stats;

    try {
        stats = fs.lstatSync(filename);
    } catch(e) {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        res.write('404 Not Found\n');
        res.end();
        return;
    }

    // check if file/directory
    if (stats.isFile()) {
        var mimeType = mimeTypes[path.extname(filename).split('.').reverse()[0]];
        res.writeHead(200, {'Content-Type': mimeType});

        // Create the file stream
        var fileStream = fs.createReadStream(filename);
        fileStream.pipe(res);
    } else if(stats.isDirectory()) {
        // 302 => Redirect
        res.writeHead(302, {
            'Location': 'index.html'
        });
        res.end();
    } else {
        // 500 => Internal Server Error
        res.writeHead(500, {'Content-Type': 'text/plain'});
        res.write('500 Internal Server Error\n');
        res.end();
    }
}).listen(8001, '127.0.0.1');