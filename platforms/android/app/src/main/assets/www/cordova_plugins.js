cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cordova-plugin-geolocation.geolocation",
    "file": "plugins/cordova-plugin-geolocation/www/android/geolocation.js",
    "pluginId": "cordova-plugin-geolocation",
    "clobbers": [
      "navigator.geolocation"
    ]
  },
  {
    "id": "cordova-plugin-geolocation.PositionError",
    "file": "plugins/cordova-plugin-geolocation/www/PositionError.js",
    "pluginId": "cordova-plugin-geolocation",
    "runs": true
  },
  {
    "id": "com-sarriaroman-photoviewer.PhotoViewer",
    "file": "plugins/com-sarriaroman-photoviewer/www/PhotoViewer.js",
    "pluginId": "com-sarriaroman-photoviewer",
    "clobbers": [
      "PhotoViewer"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-whitelist": "1.3.3",
  "cordova-plugin-geolocation": "4.0.1",
  "com-sarriaroman-photoviewer": "1.2.2"
};
// BOTTOM OF METADATA
});